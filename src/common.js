const BASE_URL_KEY = "baseUrl";
const PRIVATE_TOKEN_KEY = "privateToken";
const UPDATE_EVENT_TYPE = "update";

async function getData(keys) {
  return await new Promise((resolve, reject) => {
    try {
      chrome.storage.sync.get(keys, resolve);
    }
    catch (err) {
      reject(err);
    }
  });
}
async function getBaseUrl() {
  const data = await getData([BASE_URL_KEY]);
  return data[BASE_URL_KEY];
}

async function getPrivateToken() {
  const data = await getData([PRIVATE_TOKEN_KEY]);
  return data[PRIVATE_TOKEN_KEY];
}

async function setBaseUrl(baseUrl) {
  await new Promise(resolve => chrome.storage.sync.set({ [BASE_URL_KEY]: baseUrl }, resolve));
}

async function setPrivateToken(privateToken) {
  await new Promise(resolve => chrome.storage.sync.set({ [PRIVATE_TOKEN_KEY]: privateToken }, resolve));
}

async function triggerUpdate() {
  chrome.runtime.sendMessage(UPDATE_EVENT_TYPE);
}
