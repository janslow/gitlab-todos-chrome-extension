"use strict";

const periodInMinutes = 1.0;

function wrapAsync(fn) {
  return (...args) => {
    try {
      return fn(...args);
    } catch (err) {
      console.error(err);
      chrome.browserAction.setBadgeText({ text: "ERR" });
    }
  }
}

chrome.browserAction.onClicked.addListener(wrapAsync(async function () {
  chrome.tabs.create({
    url: `${await getBaseUrl()}/dashboard/todos`,
  });
  await updateTodos(true);
}));

let lastUpdateTime = Date.now();
const notifications = new Map();
wrapAsync(updateTodos)();
wrapAsync(openOptionsIfNecessary)();

chrome.notifications.onClicked.addListener(notificationId => {
  if (!notifications.has(notificationId)) {
    console.warn(`Unknown notification with ID "${notificationId}" was clicked.`);
    return;
  }
  const todo = notifications.get(notificationId);
  chrome.tabs.create({
    url: todo.target_url,
  });
});

chrome.notifications.onClosed.addListener(notificationId => {
  if (!notifications.has(notificationId)) {
    console.warn(`Unknown notification with ID "${notificationId}" was closed.`);
    return;
  }
  notifications.delete(notificationId);
});

const ALARM_NAME = "pollGitLabTodos";
chrome.alarms.create(ALARM_NAME, {
  delayInMinutes: periodInMinutes,
  periodInMinutes,
});

chrome.alarms.onAlarm.addListener(alarm => {
  if (alarm.name === ALARM_NAME) {
    wrapAsync(updateTodos)();
  }
})

chrome.runtime.onMessage.addListener(request => {
  if (request === UPDATE_EVENT_TYPE) {
    wrapAsync(updateTodos)();
  }
})

async function updateTodos(suppressNotifications) {
  const baseUrl = await getBaseUrl();
  if (baseUrl === undefined) {
    console.warn(`baseUrl is not defined; skipping update.`);
    return;
  }
  const privateToken = await getPrivateToken();
  if (privateToken === undefined) {
    console.warn(`privateToken is not defined; skipping update.`);
    return;
  }
  const resp = await window.fetch(`${baseUrl}/api/v3/todos`, {
    headers: {
      "PRIVATE-TOKEN": privateToken,
    }
  });
  if (resp.status !== 200) {
    throw new Error(`HTTP ${resp.status} when getting todos.`);
  }
  const todos = await resp.json();
  chrome.browserAction.setBadgeText({ text: `${todos.length || ''}` })
  if (!suppressNotifications) {
    todos.filter(isNewTodo).forEach(todo => {
      chrome.notifications.create(`todo-${todo.id}`, createNotification(todo), id => notifications.set(id, todo));
    });
  }
  lastUpdateTime = Date.now();
}

function createNotification(todo) {
  const title = `${todo.author.name} ${todo.action_name} ${aOrAn(todo.target_type)} ${todo.target_type} to you.`;
  return {
    type: chrome.notifications.TemplateType.BASIC,
    iconUrl: "images/gitlab128.png",
    title,
    message: todo.body,
    contextMessage: todo.project.name_with_namespace,
  };
}

function aOrAn(noun) {
  return /^\s*[aeiou]/i.test(noun) ? "an" : "a";
}

function isNewTodo(todo) {
  const todoTime = (new Date(todo.created_at)).getTime();
  return todoTime > lastUpdateTime;
}

async function openOptionsIfNecessary() {
  const baseUrl = await getBaseUrl();
  const privateToken = await getPrivateToken();
  if (baseUrl !== undefined && privateToken !== undefined) {
    return;
  }
  chrome.tabs.create({
    url: `chrome://extensions/?options=${chrome.runtime.id}`,
  });
}
