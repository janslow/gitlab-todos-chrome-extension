"use strict";

const DEFAULT_BASE_URL = "https://gitlab.com";
const DEFAULT_TOKEN = "my-token";

const form = document.getElementById("form");
const baseUrlInput = document.getElementById("baseUrl");
const privateTokenInput = document.getElementById("privateToken");
const createPATLink = document.getElementById("createPAT");
const submitButton = document.getElementById("submit");
const allInputs = [baseUrlInput, privateTokenInput];
const errorDisplay = document.getElementById("error");

baseUrlInput.addEventListener("change", () => {
  const raw = baseUrlInput.value;
  if (raw.endsWith('/')) {
    baseUrlInput.value = raw.replace(/\/$/, '');
  }
});

function updateCreatePATLink() {
  const base = baseUrlInput.value;
  createPATLink.href = `${base.replace(/\/$/, '')}/profile/personal_access_tokens`;
  createPATLink.hidden = !baseUrlInput.reportValidity();
}
baseUrlInput.addEventListener("keyup", updateCreatePATLink);

function updateSubmitDisabled() {
  const isValid = form.reportValidity();
  const noChanges = allInputs.every(e => e.savedValue === e.value);
  submitButton.disabled = !isValid || noChanges;
  if (!isValid) {
    submitButton.value = 'Invalid';
  }
  else if (noChanges) {
    submitButton.value = 'No Changes';
  }
  else {
    submitButton.value = 'Save';
  }
}
form.addEventListener("keyup", updateSubmitDisabled);

form.addEventListener("submit", async function (e) {
  e.preventDefault();

  const baseUrl = baseUrlInput.value;
  const privateToken = privateTokenInput.value;
  const url = `${baseUrl}/api/v3/todos`
  try {
    [...allInputs, submitButton].forEach(e => e.disabled = true);
    submitButton.value = 'Testing...';
    const response = await fetch(url, {
      headers: {
        "PRIVATE-TOKEN": privateToken,
      },
    });
    if (response.status !== 200) {
      throw new Error(`HTTP ${response.status}`);
    }
    await setBaseUrl(baseUrl);
    await setPrivateToken(privateToken);
    triggerUpdate();
    await init();
    submitButton.value = 'Saved';
  }
  catch (err) {
    console.error(err);
    error.innerText = `Error trying to access GitLab API (${url}) with provided token. Check the access token and base URL are correct.`;

    allInputs.forEach(e => e.disabled = false);
    updateSubmitDisabled();
  }
})

async function init() {
  baseUrlInput.savedValue = await getBaseUrl();
  baseUrlInput.value = `${baseUrlInput.savedValue || DEFAULT_BASE_URL}`;
  baseUrlInput.disabled = false;

  privateTokenInput.savedValue = await getPrivateToken();
  privateTokenInput.value = `${privateTokenInput.savedValue || DEFAULT_TOKEN}`;
  privateTokenInput.disabled = false;

  updateCreatePATLink();
  updateSubmitDisabled();
}
init();

