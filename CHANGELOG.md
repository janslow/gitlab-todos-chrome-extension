# GitLab Todos Chrome Extension

## 0.2.1
* Transpile with babel so it works with chrome without `#enable-javascript-harmony`

## 0.2.0
* Display options page on start if no options are configured.

## 0.1.0
* Initial Release
